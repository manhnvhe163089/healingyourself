﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HealingYourSelf_API.Models;
using HealingYourSelf_API.DTO;



namespace HealingYourSelf_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly HealingYourSelfContext _context;

        public AdminController(HealingYourSelfContext context)
        {
            _context = context;
        }

        // GET: api/Admin
        [HttpGet("GetAllOperator")]
        public async Task<ActionResult<IEnumerable<OperatorInfor>>> GetAllOperatorInfors()
        {
              if (_context.PersonalInfors == null)
              {
                  return NotFound();
              }
            var data = await _context.PersonalInfors.Where(a => a.RoleId == 2).ToListAsync();

            List<OperatorInfor> listOperator = new List<OperatorInfor>();
            listOperator = data.Select(x => new OperatorInfor
            {
                OperatorId = x.PersonalId,
                Username = x.Username,
                Password = x.Password,
                FullName = x.FullName,
                Address = x.Address,
                Email = x.Email,
                NickName = x.NickName,
                PhoneNumber = x.PhoneNumber,
                Dob = x.Dob,
                Gender = x.Gender
            }).ToList();
            return listOperator;
        }

        // GET: api/Admin/5
        [HttpGet("SearchOperator/{infor}")]
        public async Task<ActionResult<IEnumerable<OperatorInfor>>> SearchOperator(string infor)
        {
            try
            {
                if (_context.PersonalInfors == null)
                {
                    return NotFound();
                }

                var personalInfor = await _context.PersonalInfors
                    .Where(a => (a.Username.Contains(infor) || a.FullName.Contains(infor) || a.PhoneNumber.Contains(infor)) && a.RoleId == 2)
                    .ToListAsync();

                if (personalInfor == null)
                {
                    return NotFound();
                }

                List<OperatorInfor> operatorInfor = new List<OperatorInfor>();

                operatorInfor = personalInfor.Select(x => new OperatorInfor
                {
                    OperatorId = x.PersonalId,
                    Username = x.Username,
                    Password = x.Password,
                    FullName = x.FullName,
                    Address = x.Address,
                    Email = x.Email,
                    NickName = x.NickName,
                    PhoneNumber = x.PhoneNumber,
                    Dob = x.Dob,
                    Gender = x.Gender
                }).ToList();


                return operatorInfor;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("DetailOperator/{id}")]
        public async Task<ActionResult<OperatorInfor>> GetOperatorById(int id)
        {
            try
            {
                if (_context.PersonalInfors == null)
                {
                    return NotFound();
                }
                var personalInfor = await _context.PersonalInfors.FindAsync(id);

                if (personalInfor == null)
                {
                    return NotFound();
                }
                var operatorInfor = new OperatorInfor 
                {
                    Username = personalInfor.Username,
                    Password = personalInfor.Password,
                    FullName = personalInfor.FullName,
                    Address = personalInfor.Address,
                    Email = personalInfor.Email,
                    NickName = personalInfor.NickName,
                    PhoneNumber = personalInfor.PhoneNumber,
                    Dob = personalInfor.Dob,
                    Gender = personalInfor.Gender
                };
                return operatorInfor;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        //// PUT: api/Admin/5
        //// To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutPersonalInfor(int id, PersonalInfor personalInfor)
        //{
        //    if (id != personalInfor.PersonalId)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(personalInfor).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!PersonalInforExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        // POST: api/Admin
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("AddOperator")]
        public async Task<ActionResult<PersonalInfor>> AddOperator(OperatorInfor operatorInfor)
        {
          if (_context.PersonalInfors == null)
          {
              return Problem("Entity set 'HealingYourSelfContext.PersonalInfors'  is null.");
          }
            PersonalInfor newOperator = new PersonalInfor
            {
                Username = operatorInfor.Username,
                Password = operatorInfor.Password,
                FullName = operatorInfor.FullName,
                Address = operatorInfor.Address,
                Email = operatorInfor.Email,
                RoleId = 2, //role operator has id = 3
                NickName = operatorInfor.NickName,
                PhoneNumber = operatorInfor.PhoneNumber,
                Dob = operatorInfor.Dob,
                Gender = operatorInfor.Gender
            };
            _context.PersonalInfors.Add(newOperator);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Add new operator success", new { id = newOperator.PersonalId }, newOperator);
        }

        // DELETE: api/Admin/5
        [HttpDelete("DeleteOperator/{id}")]
        public async Task<IActionResult> DeleteOperator(int id)
        {
            if (_context.PersonalInfors == null)
            {
                return NotFound();
            }
            var personalInfor = await _context.PersonalInfors.FindAsync(id);
            if (personalInfor == null)
            {
                return NotFound();
            }

            _context.PersonalInfors.Remove(personalInfor);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool OperatorExist(int id)
        {
            return (_context.PersonalInfors?.Any(e => e.PersonalId == id)).GetValueOrDefault();
        }
    }
}
