﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HealingYourSelf_API.Models;
using System.Security.Principal;

using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using HealingYourSelf_API.DTO;
using Microsoft.Extensions.Options;
using HealingYourSelf_API.Service;
using HealingYourSelf_API.IService;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace HealingYourSelf_API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class PersonalInforsController : ControllerBase
    {
        private readonly HealingYourSelfContext _context;
        private readonly AppSettings _appSettings;
        //private readonly IUserService _userService;
        private readonly IConfiguration _configuration;
        private readonly string _secret;
        private readonly IEmailService _emailService;
        public PersonalInforsController(HealingYourSelfContext context, IOptionsMonitor<AppSettings> optionsMonitor, IConfiguration config, IEmailService emailService)
        {
            _context = context;
            _appSettings = optionsMonitor.CurrentValue;
            _configuration = config;
            _emailService = emailService;
        }

        // GET: api/PersonalInfor
        [HttpGet]

        [Authorize(Roles = "1")]
        public async Task<ActionResult<IEnumerable<PersonalInfor>>> GetPersonalInfors()
        {
            if (_context.PersonalInfors == null)
            {
                return NotFound();
            }
            return await _context.PersonalInfors.ToListAsync();
        }

        // GET: api/PersonalInfor/
        [HttpGet("{id}")]
        public async Task<ActionResult<PersonalInfor>> GetPersonalInfors(int id)
        {
            if (_context.PersonalInfors == null)
            {
                return NotFound();
            }
            var personalInfor = await _context.PersonalInfors.FindAsync(id);

            if (personalInfor == null)
            {
                return NotFound();
            }

            return personalInfor;
        }
        //--------------------------------------------------
        // GET: api/PersonalInfor/
        [HttpGet("Profile/{id}")]
        public async Task<ActionResult<EditProfile>> GetPersonalProfile(int id)
        {
            if (_context.PersonalInfors == null)
            {
                return NotFound();
            }
            var personalInfor = await _context.PersonalInfors.FindAsync(id);

            var personalProfile = new EditProfile
            {
                FullName = personalInfor.FullName,
                NickName = personalInfor.NickName,
                Address = personalInfor.Address,
                Dob = personalInfor.Dob,
                Email = personalInfor.Email,
                PhoneNumber = personalInfor.PhoneNumber

            };
            if (personalInfor == null)
            {
                return NotFound();
            }

            return personalProfile;
        }
        [HttpPut("EditProfile/{id}")]
        public async Task<ActionResult<EditProfile>> EditProfile(EditProfile editProfile, int id)
        {
            if (_context.PersonalInfors == null)
            {
                return NotFound();
            }
            var personalInfor = await _context.PersonalInfors.FindAsync(id);


            personalInfor.FullName = editProfile.FullName;
            personalInfor.NickName = editProfile.NickName;
            personalInfor.Address = editProfile.Address;
            personalInfor.Dob = editProfile.Dob;
            personalInfor.Email = editProfile.Email;
            personalInfor.PhoneNumber = editProfile.PhoneNumber;


            _context.SaveChanges();
            if (personalInfor == null)
            {
                return NotFound();
            }

            return Ok("Update success");
        }


        //----tl
        // PUT: api/PersonalInfors/
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPersonalInfors(int id, PersonalInfor personalInfor)
        {
            if (id != personalInfor.PersonalId)
            {
                return BadRequest();
            }

            _context.Entry(personalInfor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PersonalInforsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Accounts
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<PersonalInfor>> PostPersonalInfors(PersonalInfor personalInfor)
        {
            if (_context.PersonalInfors == null)
            {
                return Problem("Entity set 'HealingYourSelfContext.Users'  is null.");
            }
            _context.PersonalInfors.Add(personalInfor);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPersonalInfor", new { id = personalInfor.PersonalId }, personalInfor);
        }

        // DELETE: api/Accounts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePersonalInfors(int id)
        {
            if (_context.PersonalInfors == null)
            {
                return NotFound();
            }
            var personalInfor = await _context.PersonalInfors.FindAsync(id);
            if (personalInfor == null)
            {
                return NotFound();
            }

            _context.PersonalInfors.Remove(personalInfor);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool PersonalInforsExists(int id)
        {
            return (_context.PersonalInfors?.Any(e => e.PersonalId == id)).GetValueOrDefault();
        }
        //--------------------------------------------
        [HttpPost("login")]
        public IActionResult Login([FromBody] LoginModel loginModel)
        {
            var personalInfors = _context.PersonalInfors.FirstOrDefault(x => x.Username == loginModel.Username && x.Password == loginModel.Password.ToLower());
            if (personalInfors != null)
            {
                // Tạo token
                var token = GenerateJwtToken(personalInfors);


                return Ok(new { Token = token, Role = personalInfors.RoleId, PersonalId = personalInfors.PersonalId });
            }
            return Unauthorized();
        }
        private string GenerateJwtToken(PersonalInfor person)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, person.PersonalId.ToString()),
                new Claim(ClaimTypes.Role, person.RoleId.ToString())

            };
            var token = new JwtSecurityToken(
                issuer: _configuration["Jwt:Issuer"],
                audience: _configuration["Jwt:Audience"],
                claims,
                expires: DateTime.Now.AddMinutes(Convert.ToDouble(_configuration["Jwt:ExpiryMinutes"])),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
        [HttpPost("register")]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            await _context.PersonalInfors.AddAsync(new PersonalInfor
            {
                Username = model.Username,
                Password = model.Password,
                RoleId = 4,
                FullName = model.FullName,
                NickName = model.NickName,
                Email = model.Email,
                PhoneNumber = model.PhoneNumber,
                Address = model.Address,
                Gender = model.Gender,
                Dob = model.Dob,
            });
            await _context.SaveChangesAsync();
            return Ok("okok");
        }

        [HttpPost("forgot-password")]
        public async Task<IActionResult> ForgotPassword(string email)
        {

            var user = await _context.PersonalInfors.FirstOrDefaultAsync(x => x.Email == email);
            if (user == null)
            {
                return BadRequest("ok");
            }
            _emailService.SendEmail(email, "Reset Password", "Reset password, Click <a href=\"https://localhost:7024/authen/resetpassword?email=" + email + "\">Here</a>");
            return Ok("Send new password successfully");
        }
        [HttpPost("reset-password")]
        public async Task<IActionResult> ResetPassword(string email, ResetPasswordViewModel model)
        {
            var user = await _context.PersonalInfors.FirstOrDefaultAsync(x => x.Email == email);
            if (user == null)
            {
                return BadRequest("!!!!");
            }
            model.Email = email;
            user.Password = model.NewPassword;
            await _context.SaveChangesAsync();
            return Ok("Change password successfully");
        }
        // GET: api/PersonalInfor
        //[HttpGet]

        //[Authorize(Roles = "1")]
        //public async Task<ActionResult<IEnumerable<PersonalInfor>>> GetPersonalInfors()
        //{
        //    if (_context.PersonalInfors == null)
        //    {
        //        return NotFound();
        //    }
        //    return await _context.PersonalInfors.ToListAsync();
        //}


        //private string GeneratePassword()
        //{
        //    const string validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        //    StringBuilder password = new StringBuilder();
        //    Random random = new Random();

        //    for (int i = 0; i < 8; i++)
        //    {
        //        int randomIndex = random.Next(0, validChars.Length);
        //        password.Append(validChars[randomIndex]);
        //    }

        //    return password.ToString();
        //}
    }
}
