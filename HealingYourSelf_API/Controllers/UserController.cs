﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HealingYourSelf_API.Models;
using HealingYourSelf_API.DTO;



namespace HealingYourSelf_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly HealingYourSelfContext _context;

        public UserController(HealingYourSelfContext context)
        {
            _context = context;
        }

        // GET: api/User
        [HttpGet("GetAllUser")]
        public async Task<ActionResult<IEnumerable<UserInfo>>> GetAllUserInfors()
        {
            
            //var data = await _context.PersonalInfors.Where(a => a.RoleId == 4).ToListAsync();
            var joinedData = from od in _context.PersonalInfors.Where(a => a.RoleId == 4)
                             join p in _context.Users on od.PersonalId equals p.PersonalId
                             select new { PersonalInfor = od, User = p };
            if (joinedData == null)
            {
                return NotFound();
            }
            List<UserInfo> listUsers = new List<UserInfo>();
            listUsers = joinedData.Select(x => new UserInfo
            {
               UserID = x.PersonalInfor.PersonalId,
                Username = x.PersonalInfor.Username,
                Password = x.PersonalInfor.Password,
                FullName = x.PersonalInfor.FullName,
                Address = x.PersonalInfor.Address,
                Email = x.PersonalInfor.Email,
                NickName = x.PersonalInfor.NickName,
                PhoneNumber = x.PersonalInfor.PhoneNumber,
                Dob = x.PersonalInfor.Dob,
                Gender = x.PersonalInfor.Gender,
                Occupation = x.User.Occupation,
                Cause = x.User.Cause

            }).ToList();
            return listUsers;
        }
        //--------------------------------------
        [HttpGet("GetUserById/{id}")]
        public async Task<ActionResult<IEnumerable<UserInfo>>> GetUserById(int id)
        {
           
              
                //var UserInfor = await _context.PersonalInfors.FirstOrDefaultAsync(a => a.PersonalId == id && a.RoleId == 4);
                var joinedData = from od in _context.PersonalInfors.Where(x => x.PersonalId == id && x.RoleId == 4)
                                 join p in _context.Users on od.PersonalId equals p.PersonalId
                                 select new { PersonalInfor = od, User = p };
                if (joinedData == null)
                {
                    return NotFound();
                }
                List<UserInfo> listUsers = new List<UserInfo>();
                listUsers = joinedData.Select(x => new UserInfo
                {
                    UserID = x.PersonalInfor.PersonalId,
                    Username = x.PersonalInfor.Username,
                    Password = x.PersonalInfor.Password,
                    FullName = x.PersonalInfor.FullName,
                    Address = x.PersonalInfor.Address,
                    Email = x.PersonalInfor.Email,
                    NickName = x.PersonalInfor.NickName,
                    PhoneNumber = x.PersonalInfor.PhoneNumber,
                    Dob = x.PersonalInfor.Dob,
                    Gender = x.PersonalInfor.Gender,
                    Occupation = x.User.Occupation,
                    Cause = x.User.Cause

                }).ToList();
                return listUsers;
           
        }
        //------------------------------------

        // GET: api/Operator/5
        [HttpGet("SearchUser/{infor}")]
        public async Task<ActionResult<IEnumerable<UserInfo>>> SearchUser(string infor)
        {
            try
            {
                

                //var userInfo = await _context.PersonalInfors
                //    .Where(a => (a.Username.Contains(infor) || a.FullName.Contains(infor) || a.PhoneNumber.Contains(infor)) && a.RoleId == 4)
                //    .ToListAsync();
                var joinedData = from od in _context.PersonalInfors.Where(a => (a.Username.Contains(infor) || a.FullName.Contains(infor) || a.PhoneNumber.Contains(infor)) && a.RoleId == 4)
                                 join p in _context.Users on od.PersonalId equals p.PersonalId
                                 select new { PersonalInfor = od, User = p };
                if (joinedData == null)
                {                                           
                    return NotFound();
                }

                List<UserInfo> userInfor = new List<UserInfo>();

                userInfor = joinedData.Select(x => new UserInfo
                {
                    UserID = x.PersonalInfor.PersonalId,
                    Username = x.PersonalInfor.Username,
                    Password = x.PersonalInfor.Password,
                    FullName = x.PersonalInfor.FullName,
                    Address = x.PersonalInfor.Address,
                    Email = x.PersonalInfor.Email,
                    NickName = x.PersonalInfor.NickName,
                    PhoneNumber = x.PersonalInfor.PhoneNumber,
                    Dob = x.PersonalInfor.Dob,
                    Gender = x.PersonalInfor.Gender,
                    Occupation = x.User.Occupation,
                    Cause = x.User.Cause
                }).ToList();


                return userInfor;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        
        //// PUT: api/Admin/5
        //// To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutPersonalInfor(int id, PersonalInfor personalInfor)
        //{
        //    if (id != personalInfor.PersonalId)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(personalInfor).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!PersonalInforExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        // POST: api/Operator
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("AddUser")]
        public async Task<ActionResult<UserInfo>> AddUser(UserInfo userInfo)
        {
            var joinedData = from od in _context.PersonalInfors.Where(a => a.Username == userInfo.Username && a.RoleId == 4)
                             join p in _context.Users on od.PersonalId equals p.PersonalId
                             select new { PersonalInfor = od, User = p };
            if (joinedData != null)
            {
                return BadRequest("User with the same username already exists.");
            }
            var newUser = new UserInfo
            {
                UserID = userInfo.UserID,
                Username = userInfo.Username,
                Password = userInfo.Password,
                FullName = userInfo.FullName,
                Address = userInfo.Address,
                Email = userInfo.Email,
                NickName = userInfo.NickName,
                PhoneNumber = userInfo.PhoneNumber,
                Dob = userInfo.Dob,
                Gender = userInfo.Gender,
                Occupation = userInfo.Occupation,
                Cause = userInfo.Cause
            };

            _context.Add(newUser);
            
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUser", new { id = newUser.UserID }, newUser);
        }

        // DELETE: api/Operator/5
        [HttpDelete("DeleteUser/{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            if (_context.PersonalInfors == null)
            {
                return NotFound();
            }
            var personalInfor = await _context.PersonalInfors.FirstOrDefaultAsync(a => a.PersonalId == id && a.RoleId == 4);

            if (personalInfor == null)
            {
                return NotFound();
            }

            _context.PersonalInfors.Remove(personalInfor);
            await _context.SaveChangesAsync();

            return NoContent();
            
        }


        //[HttpPost("Ban/{id}")]
        //public async Task<IActionResult> BanAccountUser(int id)
        //{
        //    // Lấy thông tin tài khoản cần ban
        //    var account = await _context.PersonalInfors.FindAsync(id);
        //    if (account == null)
        //    {
        //        return NotFound();
        //    }

        //    // Kiểm tra quyền của người thực hiện
        //    if (!User.IsInRole("Admin"))
        //    {
        //        return Forbid(); // Người dùng không có quyền thực hiện hành động này
        //    }

        //    // Cập nhật trạng thái tài khoản thành "Banned"
        //    account.Status = AccountStatus.Banned;
        //    await _context.SaveChangesAsync();

        //    return Ok();
        //}

        private bool UserExist(int id)
        {
            return (_context.PersonalInfors?.Any(e => e.PersonalId == id)).GetValueOrDefault();
        }
    }
}
