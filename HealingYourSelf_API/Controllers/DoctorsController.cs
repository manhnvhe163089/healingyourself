﻿using HealingYourSelf_API.DTO;
using HealingYourSelf_API.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HealingYourSelf_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DoctorsController : ControllerBase
    {
        private readonly HealingYourSelfContext _context;

        public DoctorsController(HealingYourSelfContext context)
        {
            _context = context;
        }

        // GET: api/Doctor
        [HttpGet("GetAllDoctor")]
        public async Task<ActionResult<IEnumerable<DoctorInfo>>> GetAllDoctorInfors()
        {

            var joinedData = from od in _context.PersonalInfors.Where(a => a.RoleId == 3)
                             join p in _context.Doctors on od.PersonalId equals p.PersonalId
                             select new { PersonalInfor = od, Doctor = p };
            if (joinedData == null)
            {
                return NotFound();
            }
            List<DoctorInfo> listDoctors = new List<DoctorInfo>();
            listDoctors = joinedData.Select(x => new DoctorInfo
            {
                DoctorID = x.PersonalInfor.PersonalId,
                Username = x.PersonalInfor.Username,
                Password = x.PersonalInfor.Password,
                FullName = x.PersonalInfor.FullName,
                Address = x.PersonalInfor.Address,
                Email = x.PersonalInfor.Email,
                NickName = x.PersonalInfor.NickName,
                PhoneNumber = x.PersonalInfor.PhoneNumber,
                Dob = x.PersonalInfor.Dob,
               Certificate = x.Doctor.Certificate,
               Status = x.Doctor.Status,
               YearOfExperience = x.Doctor.YearOfExperience,
               Specialization = x.Doctor.Specialization

            }).ToList();
            return listDoctors;
        }
        //--------------------------------------
        [HttpGet("GetDoctorById/{id}")]
        public async Task<ActionResult<IEnumerable<DoctorInfo>>> GetDoctorById(int id)
        {


            var joinedData = from od in _context.PersonalInfors.Where(x => x.PersonalId == id && x.RoleId == 3)
                             join p in _context.Doctors on od.PersonalId equals p.PersonalId
                             select new { PersonalInfor = od, Doctor = p };
            if (joinedData == null)
            {
                return NotFound();
            }
            List<DoctorInfo> listDoctors = new List<DoctorInfo>();
            listDoctors = joinedData.Select(x => new DoctorInfo
            {
                DoctorID = x.PersonalInfor.PersonalId,
                Username = x.PersonalInfor.Username,
                Password = x.PersonalInfor.Password,
                FullName = x.PersonalInfor.FullName,
                Address = x.PersonalInfor.Address,
                Email = x.PersonalInfor.Email,
                NickName = x.PersonalInfor.NickName,
                PhoneNumber = x.PersonalInfor.PhoneNumber,
                Dob = x.PersonalInfor.Dob,
                Certificate = x.Doctor.Certificate,
                Status = x.Doctor.Status,
                YearOfExperience = x.Doctor.YearOfExperience,
                Specialization = x.Doctor.Specialization

            }).ToList();
            return listDoctors;

        }
        //------------------------------------

        // GET: api/Operator/5
        [HttpGet("SearchDoctor/{infor}")]
        public async Task<ActionResult<IEnumerable<DoctorInfo>>> SearchDoctor(string infor)
        {
            try
            {




                var joinedData = from od in _context.PersonalInfors.Where(a => (a.Username.Contains(infor) || a.FullName.Contains(infor) || a.PhoneNumber.Contains(infor)) && a.RoleId == 3)
                                 join p in _context.Doctors on od.PersonalId equals p.PersonalId
                                 select new { PersonalInfor = od, Doctor = p };
                if (joinedData == null)
                {
                    return NotFound();
                }

                List<DoctorInfo> doctorInfor = new List<DoctorInfo>();

                doctorInfor = joinedData.Select(x => new DoctorInfo
                {

                    DoctorID = x.PersonalInfor.PersonalId,
                    Username = x.PersonalInfor.Username,
                    Password = x.PersonalInfor.Password,
                    FullName = x.PersonalInfor.FullName,
                    Address = x.PersonalInfor.Address,
                    Email = x.PersonalInfor.Email,
                    NickName = x.PersonalInfor.NickName,
                    PhoneNumber = x.PersonalInfor.PhoneNumber,
                    Dob = x.PersonalInfor.Dob,
                    Certificate = x.Doctor.Certificate,
                    Status = x.Doctor.Status,
                    YearOfExperience = x.Doctor.YearOfExperience,
                    Specialization = x.Doctor.Specialization
                }).ToList();


                return doctorInfor;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        //// PUT: api/Admin/5
        //// To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutPersonalInfor(int id, PersonalInfor personalInfor)
        //{
        //    if (id != personalInfor.PersonalId)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(personalInfor).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!PersonalInforExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        // POST: api/Operator
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("AddDoctor")]
        public async Task<ActionResult<DoctorInfo>> AddDoctor(DoctorInfo doctorInfo)
        {
            var joinedData = from od in _context.PersonalInfors.Where(a => a.Username == doctorInfo.Username && a.RoleId == 3)
                             join p in _context.Doctors on od.PersonalId equals p.PersonalId
                             select new { PersonalInfor = od, Doctor = p };
            if (joinedData != null)
            {
                return BadRequest("Doctor with the same username already exists.");
            }
            var newDoctor = new DoctorInfo
            {
                DoctorID = doctorInfo.DoctorID,
                Username = doctorInfo.Username,
                Password = doctorInfo.Password,
                FullName = doctorInfo.FullName,
                Address = doctorInfo.Address,
                Email = doctorInfo.Email,
                NickName = doctorInfo.NickName,
                PhoneNumber = doctorInfo.PhoneNumber,
                Dob = doctorInfo.Dob,
                Certificate = doctorInfo.Certificate,
                Status = doctorInfo.Status,
                YearOfExperience = doctorInfo.YearOfExperience,
                Specialization = doctorInfo.Specialization
            };

            _context.Add(newDoctor);

            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDoctor", new { id = newDoctor.DoctorID }, newDoctor);
        }
        // DELETE: api/Operator/5
        [HttpDelete("DeleteDoctor/{id}")]
        public async Task<IActionResult> DeleteDoctor(int id)
        {
            if (_context.PersonalInfors == null)
            {
                return NotFound();
            }
            var personalInfor = await _context.PersonalInfors.FirstOrDefaultAsync(a => a.PersonalId == id && a.RoleId == 3);

            if (personalInfor == null)
            {
                return NotFound();
            }

            _context.PersonalInfors.Remove(personalInfor);
            await _context.SaveChangesAsync();

            return NoContent();
        }


        private bool UserExist(int id)
        {
            return (_context.PersonalInfors?.Any(e => e.PersonalId == id)).GetValueOrDefault();
        }
    }
}
