﻿namespace HealingYourSelf_API.IService
{
    public interface IEmailService
    {
        void SendEmail(string to, string subject, string content);
    }
}
