﻿using System.ComponentModel.DataAnnotations;

namespace HealingYourSelf_API.DTO
{
    public class UserInfo
    {
        public int UserID { get; set; }
        public string Username { get; set; } = null!;
        public string Password { get; set; } = null!;
        public string? FullName { get; set; }
        public string? NickName { get; set; }
        [EmailAddress]
        public string? Email { get; set; }
        public string? PhoneNumber { get; set; }
        public string? Address { get; set; }
        public string? Gender { get; set; }
        public DateTime? Dob { get; set; }
        public string? Occupation { get; set; }
        public string? Cause { get; set; }

    }
}
