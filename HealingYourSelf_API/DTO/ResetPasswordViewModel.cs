﻿using System.ComponentModel.DataAnnotations;

namespace HealingYourSelf_API.DTO
{
    public class ResetPasswordViewModel
    {
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string NewPassword { get; set; }
        [Required, Compare("NewPassword")]
        public string ConfirmNewPassword { get; set; }
    }
}
