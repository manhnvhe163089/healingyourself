﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace HealingYourSelf_API.Models
{
    public partial class HealingYourSelfContext : DbContext
    {
        public HealingYourSelfContext()
        {
        }

        public HealingYourSelfContext(DbContextOptions<HealingYourSelfContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Blog> Blogs { get; set; } = null!;
        public virtual DbSet<BookingRequest> BookingRequests { get; set; } = null!;
        public virtual DbSet<Chat> Chats { get; set; } = null!;
        public virtual DbSet<CommentBlog> CommentBlogs { get; set; } = null!;
        public virtual DbSet<CommentOnPost> CommentOnPosts { get; set; } = null!;
        public virtual DbSet<DailyTimetable> DailyTimetables { get; set; } = null!;
        public virtual DbSet<Doctor> Doctors { get; set; } = null!;
        public virtual DbSet<Feedback> Feedbacks { get; set; } = null!;
        public virtual DbSet<Notification> Notifications { get; set; } = null!;
        public virtual DbSet<PatientStatusProfile> PatientStatusProfiles { get; set; } = null!;
        public virtual DbSet<PersonalInfor> PersonalInfors { get; set; } = null!;
        public virtual DbSet<PersonalTreatmentMethod> PersonalTreatmentMethods { get; set; } = null!;
        public virtual DbSet<Post> Posts { get; set; } = null!;
        public virtual DbSet<ReplyBlog> ReplyBlogs { get; set; } = null!;
        public virtual DbSet<ReplyOnPost> ReplyOnPosts { get; set; } = null!;
        public virtual DbSet<ReportBlog> ReportBlogs { get; set; } = null!;
        public virtual DbSet<ReportOnPost> ReportOnPosts { get; set; } = null!;
        public virtual DbSet<Role> Roles { get; set; } = null!;
        public virtual DbSet<TreatmentMethod> TreatmentMethods { get; set; } = null!;
        public virtual DbSet<User> Users { get; set; } = null!;
        public virtual DbSet<WeekTimetable> WeekTimetables { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var conf = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer(conf.GetConnectionString("DbConnection"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Blog>(entity =>
            {
                entity.ToTable("Blog");

                entity.Property(e => e.BlogId).HasColumnName("BlogID");

                entity.Property(e => e.DoctorId).HasColumnName("DoctorID");

                entity.Property(e => e.PublicDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.Property(e => e.Title).HasMaxLength(255);

                entity.HasOne(d => d.Doctor)
                    .WithMany(p => p.Blogs)
                    .HasForeignKey(d => d.DoctorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Blog__DoctorID__5535A963");
            });

            modelBuilder.Entity<BookingRequest>(entity =>
            {
                entity.HasKey(e => e.BookingId)
                    .HasName("PK__Booking __73951ACD421E3996");

                entity.ToTable("Booking Request");

                entity.Property(e => e.BookingId).HasColumnName("BookingID");

                entity.Property(e => e.BookingTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DailyId).HasColumnName("DailyID");

                entity.Property(e => e.DoctorId).HasColumnName("DoctorID");

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.Daily)
                    .WithMany(p => p.BookingRequests)
                    .HasForeignKey(d => d.DailyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Booking R__Daily__01142BA1");

                entity.HasOne(d => d.Doctor)
                    .WithMany(p => p.BookingRequests)
                    .HasForeignKey(d => d.DoctorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Booking R__Docto__7F2BE32F");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.BookingRequests)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Booking R__UserI__00200768");
            });

            modelBuilder.Entity<Chat>(entity =>
            {
                entity.ToTable("Chat");

                entity.Property(e => e.ChatId).HasColumnName("ChatID");
            });

            modelBuilder.Entity<CommentBlog>(entity =>
            {
                entity.HasKey(e => e.CommentId)
                    .HasName("PK__CommentB__C3B4DFAABB441315");

                entity.ToTable("CommentBlog");

                entity.Property(e => e.CommentId).HasColumnName("CommentID");

                entity.Property(e => e.BlogId).HasColumnName("BlogID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.Blog)
                    .WithMany(p => p.CommentBlogs)
                    .HasForeignKey(d => d.BlogId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__CommentBl__BlogI__59063A47");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.CommentBlogs)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__CommentBl__UserI__59FA5E80");
            });

            modelBuilder.Entity<CommentOnPost>(entity =>
            {
                entity.HasKey(e => e.CommentId)
                    .HasName("PK__CommentO__C3B4DFAADDC6B638");

                entity.ToTable("CommentOnPost");

                entity.Property(e => e.CommentId).HasColumnName("CommentID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PersonalId).HasColumnName("PersonalID");

                entity.Property(e => e.PostId).HasColumnName("PostID");

                entity.HasOne(d => d.Personal)
                    .WithMany(p => p.CommentOnPosts)
                    .HasForeignKey(d => d.PersonalId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__CommentOn__Perso__46E78A0C");

                entity.HasOne(d => d.Post)
                    .WithMany(p => p.CommentOnPosts)
                    .HasForeignKey(d => d.PostId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__CommentOn__PostI__47DBAE45");
            });

            modelBuilder.Entity<DailyTimetable>(entity =>
            {
                entity.HasKey(e => e.DailyId)
                    .HasName("PK__Daily Ti__650EDFF70D760E2B");

                entity.ToTable("Daily Timetable");

                entity.Property(e => e.DailyId).HasColumnName("DailyID");

                entity.Property(e => e.DoctorId).HasColumnName("DoctorID");

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.Property(e => e.TimeSlot).HasMaxLength(50);

                entity.Property(e => e.WeekId).HasColumnName("WeekID");

                entity.HasOne(d => d.Doctor)
                    .WithMany(p => p.DailyTimetables)
                    .HasForeignKey(d => d.DoctorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Daily Tim__Docto__7C4F7684");

                entity.HasOne(d => d.Week)
                    .WithMany(p => p.DailyTimetables)
                    .HasForeignKey(d => d.WeekId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Daily Tim__WeekI__7B5B524B");
            });

            modelBuilder.Entity<Doctor>(entity =>
            {
                entity.ToTable("Doctor");

                entity.Property(e => e.DoctorId).HasColumnName("DoctorID");

                entity.Property(e => e.Certificate).HasMaxLength(255);

                entity.Property(e => e.PersonalId).HasColumnName("PersonalID");

                entity.Property(e => e.Specialization).HasMaxLength(100);

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.HasOne(d => d.Personal)
                    .WithMany(p => p.Doctors)
                    .HasForeignKey(d => d.PersonalId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Doctor__Personal__403A8C7D");
            });

            modelBuilder.Entity<Feedback>(entity =>
            {
                entity.ToTable("Feedback");

                entity.Property(e => e.FeedbackId).HasColumnName("FeedbackID");

                entity.Property(e => e.DoctorId)
                    .HasColumnName("DoctorID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.FeedbackDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.Doctor)
                    .WithMany(p => p.Feedbacks)
                    .HasForeignKey(d => d.DoctorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Feedback_Doctor");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Feedbacks)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Feedback__UserID__114A936A");
            });

            modelBuilder.Entity<Notification>(entity =>
            {
                entity.ToTable("Notification");

                entity.Property(e => e.NotificationId).HasColumnName("NotificationID");

                entity.Property(e => e.Date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PersonalId).HasColumnName("PersonalID");

                entity.Property(e => e.Title).HasMaxLength(255);

                entity.HasOne(d => d.Personal)
                    .WithMany(p => p.Notifications)
                    .HasForeignKey(d => d.PersonalId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Notificat__Perso__75A278F5");
            });

            modelBuilder.Entity<PatientStatusProfile>(entity =>
            {
                entity.HasKey(e => e.PspId)
                    .HasName("PK__Patient __3FA9CAA453D5C71A");

                entity.ToTable("Patient Status Profile");

                entity.Property(e => e.PspId).HasColumnName("PSP_ID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DoctorId).HasColumnName("DoctorID");

                entity.Property(e => e.PersonalTreatmentMethodId).HasColumnName("PersonalTreatmentMethodID");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.Doctor)
                    .WithMany(p => p.PatientStatusProfiles)
                    .HasForeignKey(d => d.DoctorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Patient S__Docto__70DDC3D8");

                entity.HasOne(d => d.PersonalTreatmentMethod)
                    .WithMany(p => p.PatientStatusProfiles)
                    .HasForeignKey(d => d.PersonalTreatmentMethodId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Patient S__Perso__72C60C4A");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.PatientStatusProfiles)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Patient S__UserI__6FE99F9F");
            });

            modelBuilder.Entity<PersonalInfor>(entity =>
            {
                entity.HasKey(e => e.PersonalId)
                    .HasName("PK__Personal__283437130A9C8BB1");

                entity.ToTable("Personal Infor");

                entity.HasIndex(e => e.NickName, "UQ__Personal__01E67C8B5A975FDF")
                    .IsUnique();

                entity.HasIndex(e => e.Username, "UQ__Personal__536C85E4DE218425")
                    .IsUnique();

                entity.Property(e => e.PersonalId).HasColumnName("PersonalID");

                entity.Property(e => e.Address).HasMaxLength(255);

                entity.Property(e => e.Dob).HasColumnType("date");

                entity.Property(e => e.Email).HasMaxLength(100);

                entity.Property(e => e.FullName).HasMaxLength(100);

                entity.Property(e => e.Gender).HasMaxLength(10);

                entity.Property(e => e.NickName).HasMaxLength(50);

                entity.Property(e => e.Password).HasMaxLength(255);

                entity.Property(e => e.PhoneNumber).HasMaxLength(20);

                entity.Property(e => e.RoleId).HasColumnName("RoleID");

                entity.Property(e => e.Username).HasMaxLength(50);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.PersonalInfors)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Personal __RoleI__3A81B327");
            });

            modelBuilder.Entity<PersonalTreatmentMethod>(entity =>
            {
                entity.ToTable("Personal Treatment Method");

                entity.Property(e => e.PersonalTreatmentMethodId).HasColumnName("PersonalTreatmentMethodID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DoctorId).HasColumnName("DoctorID");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.Doctor)
                    .WithMany(p => p.PersonalTreatmentMethods)
                    .HasForeignKey(d => d.DoctorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Personal __Docto__6B24EA82");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.PersonalTreatmentMethods)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Personal __UserI__6C190EBB");
            });

            modelBuilder.Entity<Post>(entity =>
            {
                entity.ToTable("Post");

                entity.Property(e => e.PostId).HasColumnName("PostID");

                entity.Property(e => e.PublicDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.Property(e => e.Title).HasMaxLength(255);

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Posts)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Post__UserID__4316F928");
            });

            modelBuilder.Entity<ReplyBlog>(entity =>
            {
                entity.HasKey(e => e.ReplyId)
                    .HasName("PK__ReplyBlo__C25E4629CE147668");

                entity.ToTable("ReplyBlog");

                entity.Property(e => e.ReplyId).HasColumnName("ReplyID");

                entity.Property(e => e.CommentId).HasColumnName("CommentID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DoctorId).HasColumnName("DoctorID");

                entity.HasOne(d => d.Comment)
                    .WithMany(p => p.ReplyBlogs)
                    .HasForeignKey(d => d.CommentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ReplyBlog__Comme__5DCAEF64");

                entity.HasOne(d => d.Doctor)
                    .WithMany(p => p.ReplyBlogs)
                    .HasForeignKey(d => d.DoctorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ReplyBlog__Docto__5EBF139D");
            });

            modelBuilder.Entity<ReplyOnPost>(entity =>
            {
                entity.HasKey(e => e.ReplyId)
                    .HasName("PK__ReplyOnP__C25E46297DC18C96");

                entity.ToTable("ReplyOnPost");

                entity.Property(e => e.ReplyId).HasColumnName("ReplyID");

                entity.Property(e => e.CommentId).HasColumnName("CommentID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PersonalId).HasColumnName("PersonalID");

                entity.HasOne(d => d.Comment)
                    .WithMany(p => p.ReplyOnPosts)
                    .HasForeignKey(d => d.CommentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ReplyOnPo__Comme__4BAC3F29");

                entity.HasOne(d => d.Personal)
                    .WithMany(p => p.ReplyOnPosts)
                    .HasForeignKey(d => d.PersonalId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ReplyOnPo__Perso__4CA06362");
            });

            modelBuilder.Entity<ReportBlog>(entity =>
            {
                entity.HasKey(e => e.ReportId)
                    .HasName("PK__ReportBl__D5BD48E560C52CAF");

                entity.ToTable("ReportBlog");

                entity.Property(e => e.ReportId).HasColumnName("ReportID");

                entity.Property(e => e.BlogId).HasColumnName("BlogID");

                entity.Property(e => e.PersonalId).HasColumnName("PersonalID");

                entity.Property(e => e.ReportDate)
                    .HasColumnType("datetime")
                    .HasColumnName("Report_Date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.HasOne(d => d.Blog)
                    .WithMany(p => p.ReportBlogs)
                    .HasForeignKey(d => d.BlogId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ReportBlo__BlogI__628FA481");

                entity.HasOne(d => d.Personal)
                    .WithMany(p => p.ReportBlogs)
                    .HasForeignKey(d => d.PersonalId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ReportBlo__Perso__6383C8BA");
            });

            modelBuilder.Entity<ReportOnPost>(entity =>
            {
                entity.HasKey(e => e.ReportId)
                    .HasName("PK__ReportOn__D5BD48E582769E39");

                entity.ToTable("ReportOnPost");

                entity.Property(e => e.ReportId).HasColumnName("ReportID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PersonalId).HasColumnName("PersonalID");

                entity.Property(e => e.PostId).HasColumnName("PostID");

                entity.HasOne(d => d.Personal)
                    .WithMany(p => p.ReportOnPosts)
                    .HasForeignKey(d => d.PersonalId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ReportOnP__Perso__5165187F");

                entity.HasOne(d => d.Post)
                    .WithMany(p => p.ReportOnPosts)
                    .HasForeignKey(d => d.PostId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ReportOnP__PostI__5070F446");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("Role");

                entity.Property(e => e.RoleId).HasColumnName("RoleID");

                entity.Property(e => e.RoleName).HasMaxLength(50);
            });

            modelBuilder.Entity<TreatmentMethod>(entity =>
            {
                entity.ToTable("Treatment Method");

                entity.Property(e => e.TreatmentMethodId).HasColumnName("TreatmentMethodID");

                entity.Property(e => e.DoctorId).HasColumnName("DoctorID");

                entity.Property(e => e.PublicDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Title).HasMaxLength(255);

                entity.HasOne(d => d.Doctor)
                    .WithMany(p => p.TreatmentMethods)
                    .HasForeignKey(d => d.DoctorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Treatment__Docto__6754599E");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("User");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.Occupation).HasMaxLength(100);

                entity.Property(e => e.PersonalId).HasColumnName("PersonalID");

                entity.HasOne(d => d.Personal)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.PersonalId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__User__PersonalID__3D5E1FD2");
            });

            modelBuilder.Entity<WeekTimetable>(entity =>
            {
                entity.HasKey(e => e.WeekId)
                    .HasName("PK__Week Tim__C814A5E1335B908A");

                entity.ToTable("Week Timetable");

                entity.Property(e => e.WeekId).HasColumnName("WeekID");

                entity.Property(e => e.DayOfWeek).HasMaxLength(50);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
