﻿using Microsoft.AspNetCore.Identity;

namespace HealingYourSelf_API.Models
{
    public class ApplicationUser: IdentityUser
    {
    }
}
