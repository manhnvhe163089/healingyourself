﻿using System;
using System.Collections.Generic;

namespace HealingYourSelf_API.Models
{
    public partial class ReportOnPost
    {
        public int ReportId { get; set; }
        public int PostId { get; set; }
        public int PersonalId { get; set; }
        public string? Reason { get; set; }
        public DateTime? CreateDate { get; set; }

        public virtual PersonalInfor Personal { get; set; } = null!;
        public virtual Post Post { get; set; } = null!;
    }
}
