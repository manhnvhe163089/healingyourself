﻿using System;
using System.Collections.Generic;

namespace HealingYourSelf_API.Models
{
    public partial class User
    {
        public User()
        {
            BookingRequests = new HashSet<BookingRequest>();
            CommentBlogs = new HashSet<CommentBlog>();
            Feedbacks = new HashSet<Feedback>();
            PatientStatusProfiles = new HashSet<PatientStatusProfile>();
            PersonalTreatmentMethods = new HashSet<PersonalTreatmentMethod>();
            Posts = new HashSet<Post>();
        }

        public int UserId { get; set; }
        public int PersonalId { get; set; }
        public string? Occupation { get; set; }
        public string? Cause { get; set; }

        public virtual PersonalInfor Personal { get; set; } = null!;
        public virtual ICollection<BookingRequest> BookingRequests { get; set; }
        public virtual ICollection<CommentBlog> CommentBlogs { get; set; }
        public virtual ICollection<Feedback> Feedbacks { get; set; }
        public virtual ICollection<PatientStatusProfile> PatientStatusProfiles { get; set; }
        public virtual ICollection<PersonalTreatmentMethod> PersonalTreatmentMethods { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
    }
}
