﻿using System;
using System.Collections.Generic;

namespace HealingYourSelf_API.Models
{
    public partial class PatientStatusProfile
    {
        public int PspId { get; set; }
        public int UserId { get; set; }
        public int DoctorId { get; set; }
        public DateTime? CreateDate { get; set; }
        public string? Note { get; set; }
        public int PersonalTreatmentMethodId { get; set; }
        public string? StatusProfileContent { get; set; }
        public int? StressLevel { get; set; }

        public virtual Doctor Doctor { get; set; } = null!;
        public virtual PersonalTreatmentMethod PersonalTreatmentMethod { get; set; } = null!;
        public virtual User User { get; set; } = null!;
    }
}
