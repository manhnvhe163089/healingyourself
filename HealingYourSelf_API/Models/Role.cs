﻿using System;
using System.Collections.Generic;

namespace HealingYourSelf_API.Models
{
    public partial class Role
    {
        public Role()
        {
            PersonalInfors = new HashSet<PersonalInfor>();
        }

        public int RoleId { get; set; }
        public string RoleName { get; set; } = null!;

        public virtual ICollection<PersonalInfor> PersonalInfors { get; set; }
    }
}
