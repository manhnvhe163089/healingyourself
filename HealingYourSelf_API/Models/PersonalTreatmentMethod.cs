﻿using System;
using System.Collections.Generic;

namespace HealingYourSelf_API.Models
{
    public partial class PersonalTreatmentMethod
    {
        public PersonalTreatmentMethod()
        {
            PatientStatusProfiles = new HashSet<PatientStatusProfile>();
        }

        public int PersonalTreatmentMethodId { get; set; }
        public int DoctorId { get; set; }
        public int UserId { get; set; }
        public DateTime? CreateDate { get; set; }

        public virtual Doctor Doctor { get; set; } = null!;
        public virtual User User { get; set; } = null!;
        public virtual ICollection<PatientStatusProfile> PatientStatusProfiles { get; set; }
    }
}
