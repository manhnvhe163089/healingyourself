﻿using System;
using System.Collections.Generic;

namespace HealingYourSelf_API.Models
{
    public partial class PersonalInfor
    {
        public PersonalInfor()
        {
            CommentOnPosts = new HashSet<CommentOnPost>();
            Doctors = new HashSet<Doctor>();
            Notifications = new HashSet<Notification>();
            ReplyOnPosts = new HashSet<ReplyOnPost>();
            ReportBlogs = new HashSet<ReportBlog>();
            ReportOnPosts = new HashSet<ReportOnPost>();
            Users = new HashSet<User>();
        }

        public int PersonalId { get; set; }
        public string Username { get; set; } = null!;
        public string Password { get; set; } = null!;
        public int RoleId { get; set; }
        public string? FullName { get; set; }
        public string? NickName { get; set; }
        public string? Email { get; set; }
        public string? PhoneNumber { get; set; }
        public string? Address { get; set; }
        public string? Gender { get; set; }
        public DateTime? Dob { get; set; }
        public byte[]? Avatar { get; set; }

        public virtual Role Role { get; set; } = null!;
        public virtual ICollection<CommentOnPost> CommentOnPosts { get; set; }
        public virtual ICollection<Doctor> Doctors { get; set; }
        public virtual ICollection<Notification> Notifications { get; set; }
        public virtual ICollection<ReplyOnPost> ReplyOnPosts { get; set; }
        public virtual ICollection<ReportBlog> ReportBlogs { get; set; }
        public virtual ICollection<ReportOnPost> ReportOnPosts { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
