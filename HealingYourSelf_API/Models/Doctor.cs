﻿using System;
using System.Collections.Generic;

namespace HealingYourSelf_API.Models
{
    public partial class Doctor
    {
        public Doctor()
        {
            Blogs = new HashSet<Blog>();
            BookingRequests = new HashSet<BookingRequest>();
            DailyTimetables = new HashSet<DailyTimetable>();
            Feedbacks = new HashSet<Feedback>();
            PatientStatusProfiles = new HashSet<PatientStatusProfile>();
            PersonalTreatmentMethods = new HashSet<PersonalTreatmentMethod>();
            ReplyBlogs = new HashSet<ReplyBlog>();
            TreatmentMethods = new HashSet<TreatmentMethod>();
        }

        public int DoctorId { get; set; }
        public int PersonalId { get; set; }
        public string? Certificate { get; set; }
        public string? Status { get; set; }
        public int? YearOfExperience { get; set; }
        public string? Specialization { get; set; }

        public virtual PersonalInfor Personal { get; set; } = null!;
        public virtual ICollection<Blog> Blogs { get; set; }
        public virtual ICollection<BookingRequest> BookingRequests { get; set; }
        public virtual ICollection<DailyTimetable> DailyTimetables { get; set; }
        public virtual ICollection<Feedback> Feedbacks { get; set; }
        public virtual ICollection<PatientStatusProfile> PatientStatusProfiles { get; set; }
        public virtual ICollection<PersonalTreatmentMethod> PersonalTreatmentMethods { get; set; }
        public virtual ICollection<ReplyBlog> ReplyBlogs { get; set; }
        public virtual ICollection<TreatmentMethod> TreatmentMethods { get; set; }
    }
}
