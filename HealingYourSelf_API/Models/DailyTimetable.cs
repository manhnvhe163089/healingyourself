﻿using System;
using System.Collections.Generic;

namespace HealingYourSelf_API.Models
{
    public partial class DailyTimetable
    {
        public DailyTimetable()
        {
            BookingRequests = new HashSet<BookingRequest>();
        }

        public int DailyId { get; set; }
        public int WeekId { get; set; }
        public int DoctorId { get; set; }
        public string? TimeSlot { get; set; }
        public string? Status { get; set; }

        public virtual Doctor Doctor { get; set; } = null!;
        public virtual WeekTimetable Week { get; set; } = null!;
        public virtual ICollection<BookingRequest> BookingRequests { get; set; }
    }
}
