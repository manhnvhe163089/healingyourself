﻿using System;
using System.Collections.Generic;

namespace HealingYourSelf_API.Models
{
    public partial class TreatmentMethod
    {
        public int TreatmentMethodId { get; set; }
        public int DoctorId { get; set; }
        public string? Title { get; set; }
        public string? TreatmentMethodContent { get; set; }
        public DateTime? PublicDate { get; set; }

        public virtual Doctor Doctor { get; set; } = null!;
    }
}
