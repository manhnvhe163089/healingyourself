﻿using System;
using System.Collections.Generic;

namespace HealingYourSelf_API.Models
{
    public partial class ReplyOnPost
    {
        public int ReplyId { get; set; }
        public int CommentId { get; set; }
        public int PersonalId { get; set; }
        public string? ReplyContent { get; set; }
        public DateTime? CreateDate { get; set; }
        public byte[]? Attachment { get; set; }

        public virtual CommentOnPost Comment { get; set; } = null!;
        public virtual PersonalInfor Personal { get; set; } = null!;
    }
}
