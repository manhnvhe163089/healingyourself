﻿using System;
using System.Collections.Generic;

namespace HealingYourSelf_API.Models
{
    public partial class ReportBlog
    {
        public int ReportId { get; set; }
        public int BlogId { get; set; }
        public int PersonalId { get; set; }
        public string? Reason { get; set; }
        public DateTime? ReportDate { get; set; }
        public string? Status { get; set; }

        public virtual Blog Blog { get; set; } = null!;
        public virtual PersonalInfor Personal { get; set; } = null!;
    }
}
