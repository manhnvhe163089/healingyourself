﻿using System;
using System.Collections.Generic;

namespace HealingYourSelf_API.Models
{
    public partial class Blog
    {
        public Blog()
        {
            CommentBlogs = new HashSet<CommentBlog>();
            ReportBlogs = new HashSet<ReportBlog>();
        }

        public int BlogId { get; set; }
        public int DoctorId { get; set; }
        public string? Title { get; set; }
        public string? Content { get; set; }
        public DateTime? PublicDate { get; set; }
        public byte[]? Attachment { get; set; }
        public string? Status { get; set; }

        public virtual Doctor Doctor { get; set; } = null!;
        public virtual ICollection<CommentBlog> CommentBlogs { get; set; }
        public virtual ICollection<ReportBlog> ReportBlogs { get; set; }
    }
}
