﻿using System.ComponentModel.DataAnnotations;

namespace HealingYourSelf_MVC.Dto
{
    public class LoginSuccessInfo
    {
        public string Token { get; set; } = null!;
        public int Role { get; set; }
        public int PersonalId { get; set; }
    }
}
