﻿using System.ComponentModel.DataAnnotations;
namespace HealingYourSelf_MVC.Dto;

public class EditProfile
{
 
  
    public string? FullName { get; set; }
    public string? NickName { get; set; }
    [EmailAddress]
    public string? Email { get; set; }
    public string? PhoneNumber { get; set; }
    public string? Address { get; set; }

    public DateTime? Dob { get; set; }

}
