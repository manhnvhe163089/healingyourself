﻿using System.ComponentModel.DataAnnotations;

namespace HealingYourSelf_MVC.Dto
{
    public class ResetPasswordViewModel
    {
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string NewPassword { get; set; }
        [Required, Compare("NewPassword")]
        public string ConfirmNewPassword { get; set; }
    }
}
