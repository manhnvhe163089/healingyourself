﻿using System;
using System.Collections.Generic;

namespace HealingYourSelf_MVC.Models
{
    public partial class WeekTimetable
    {
        public WeekTimetable()
        {
            DailyTimetables = new HashSet<DailyTimetable>();
        }

        public int WeekId { get; set; }
        public string? DayOfWeek { get; set; }

        public virtual ICollection<DailyTimetable> DailyTimetables { get; set; }
    }
}
