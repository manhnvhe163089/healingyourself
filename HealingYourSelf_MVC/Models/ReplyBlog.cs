﻿using System;
using System.Collections.Generic;

namespace HealingYourSelf_MVC.Models
{
    public partial class ReplyBlog
    {
        public int ReplyId { get; set; }
        public int CommentId { get; set; }
        public int DoctorId { get; set; }
        public string? ReplyContent { get; set; }
        public DateTime? CreateDate { get; set; }
        public byte[]? Attachment { get; set; }

        public virtual CommentBlog Comment { get; set; } = null!;
        public virtual Doctor Doctor { get; set; } = null!;
    }
}
