﻿using System;
using System.Collections.Generic;

namespace HealingYourSelf_MVC.Models
{
    public partial class Post
    {
        public Post()
        {
            CommentOnPosts = new HashSet<CommentOnPost>();
            ReportOnPosts = new HashSet<ReportOnPost>();
        }

        public int PostId { get; set; }
        public int UserId { get; set; }
        public DateTime? PublicDate { get; set; }
        public string? Title { get; set; }
        public string? Content { get; set; }
        public byte[]? Attachment { get; set; }
        public string? Status { get; set; }

        public virtual User User { get; set; } = null!;
        public virtual ICollection<CommentOnPost> CommentOnPosts { get; set; }
        public virtual ICollection<ReportOnPost> ReportOnPosts { get; set; }
    }
}
