﻿using System;
using System.Collections.Generic;

namespace HealingYourSelf_MVC.Models
{
    public partial class CommentOnPost
    {
        public CommentOnPost()
        {
            ReplyOnPosts = new HashSet<ReplyOnPost>();
        }

        public int CommentId { get; set; }
        public int PersonalId { get; set; }
        public int PostId { get; set; }
        public DateTime? CreateDate { get; set; }
        public string? CommentContent { get; set; }
        public byte[]? Attachment { get; set; }

        public virtual PersonalInfor Personal { get; set; } = null!;
        public virtual Post Post { get; set; } = null!;
        public virtual ICollection<ReplyOnPost> ReplyOnPosts { get; set; }
    }
}
