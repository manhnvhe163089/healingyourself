﻿using System;
using System.Collections.Generic;

namespace HealingYourSelf_MVC.Models
{
    public partial class BookingRequest
    {
        public int BookingId { get; set; }
        public int DoctorId { get; set; }
        public int UserId { get; set; }
        public int DailyId { get; set; }
        public string? Status { get; set; }
        public DateTime? BookingTime { get; set; }

        public virtual DailyTimetable Daily { get; set; } = null!;
        public virtual Doctor Doctor { get; set; } = null!;
        public virtual User User { get; set; } = null!;
    }
}
