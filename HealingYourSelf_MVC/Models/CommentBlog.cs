﻿using System;
using System.Collections.Generic;

namespace HealingYourSelf_MVC.Models
{
    public partial class CommentBlog
    {
        public CommentBlog()
        {
            ReplyBlogs = new HashSet<ReplyBlog>();
        }

        public int CommentId { get; set; }
        public int BlogId { get; set; }
        public int UserId { get; set; }
        public DateTime? CreateDate { get; set; }
        public string? CommentContent { get; set; }
        public byte[]? Attachment { get; set; }

        public virtual Blog Blog { get; set; } = null!;
        public virtual User User { get; set; } = null!;
        public virtual ICollection<ReplyBlog> ReplyBlogs { get; set; }
    }
}
