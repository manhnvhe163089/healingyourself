﻿using System;
using System.Collections.Generic;

namespace HealingYourSelf_MVC.Models
{
    public partial class Feedback
    {
        public int FeedbackId { get; set; }
        public int UserId { get; set; }
        public DateTime? FeedbackDate { get; set; }
        public string? FeedbackContent { get; set; }
        public byte[]? Attachment { get; set; }
        public int DoctorId { get; set; }

        public virtual Doctor Doctor { get; set; } = null!;
        public virtual User User { get; set; } = null!;
    }
}
