﻿using System;
using System.Collections.Generic;

namespace HealingYourSelf_MVC.Models
{
    public partial class Notification
    {
        public int NotificationId { get; set; }
        public int PersonalId { get; set; }
        public string? Title { get; set; }
        public string? MessageNotification { get; set; }
        public DateTime? Date { get; set; }

        public virtual PersonalInfor Personal { get; set; } = null!;
    }
}
