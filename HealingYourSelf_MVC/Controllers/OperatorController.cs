﻿
using HealingYourSelf_MVC.Dto;
using HealingYourSelf_MVC.DTO;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Text;

namespace HealingYourSelf_MVC.Controllers
{
    public class OperatorController : Controller
    {
        private readonly HttpClient client;

        private string EditProfileUrl = "https://localhost:7051/api/PersonalInfors/Profile/";

        private string SaveChangesProfileUrl = "https://localhost:7051/api/PersonalInfors/EditProfile/";
        public OperatorController()
        {
            client = new HttpClient();
            var contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult Profile()
        {
            string id = HttpContext.Session.GetString("PersonalID");
            EditProfile editProfile = new EditProfile();
            HttpResponseMessage response = client.GetAsync(EditProfileUrl + id).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                editProfile = JsonConvert.DeserializeObject<EditProfile>(data);
            }
            return View(editProfile);
        }
        [HttpPost]
        public IActionResult Profile(EditProfile editProfile)
        {
            string data = JsonConvert.SerializeObject(editProfile);
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PutAsync(SaveChangesProfileUrl + HttpContext.Session.GetString("PersonalID"), content).Result;

            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index", "Home");
            }

            return View();

        }

        public IActionResult ManageUsers()
        {

            List<UserInfo> manageUsers = new List<UserInfo>();
            HttpResponseMessage response = client.GetAsync("https://localhost:7051/api/User/GetAllUser").Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                manageUsers = JsonConvert.DeserializeObject<List<UserInfo>>(data);
            }

            var listUsers = manageUsers.ToList();

            return View(listUsers);
        }


    }
}
