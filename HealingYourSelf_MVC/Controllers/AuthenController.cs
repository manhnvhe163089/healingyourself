﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

using HealingYourSelf_MVC.Models;
using System.Net.Http.Headers;
using System.Diagnostics.Metrics;
using System.Text.Json;
using System.Text;
using System.Security.Principal;
using static System.Net.WebRequestMethods;
using Newtonsoft.Json;
using HealingYourSelf_MVC.Dto;
using Microsoft.AspNetCore.Http;

namespace HealingYourSelf_MVC.Controllers
{
    public class AuthenController : Controller
    {
        private readonly HttpClient client;
        private string PersonalInforsUrl = "https://localhost:7051/api/PersonalInfors";
        private string LoginUrl = "https://localhost:7051/api/PersonalInfors/login";
        public AuthenController()
        {
            client = new HttpClient();
            var contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Login()
        {
            HttpContext.Session.Clear();
            return View();
        }

        public IActionResult Register()
        {
            return View();
        }



        //-----------------------
        [HttpPost]
        public async Task<IActionResult> Login(string Username, string Password)
        {
            //HttpContext.Session.Clear();
            var loginInfor = new { Username = Username, Password = Password };

            string json = System.Text.Json.JsonSerializer.Serialize(loginInfor);
            StringContent content = new StringContent(json, Encoding.UTF8, "application/json");
            try
            {

                HttpResponseMessage response = await client.PostAsync(LoginUrl, content);
                response.EnsureSuccessStatusCode();
                var options = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };
                string strData = await response.Content.ReadAsStringAsync();
                //var infoJson = System.Text.Json.JsonSerializer.Serialize(strData);
                LoginSuccessInfo info = System.Text.Json.JsonSerializer.Deserialize<LoginSuccessInfo>(strData, options);

                HttpContext.Session.SetString("Token", info.Token);
                HttpContext.Session.SetString("PersonalID", info.PersonalId.ToString());
                HttpContext.Session.SetString("Role", info.Role.ToString());
                //HttpContext.Session.Remove("Token");
                // Check if the user is an admin
                Console.WriteLine(HttpContext.Session.GetString("Token"));

                //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", HttpContext.Session.GetString("Token"));
                //HttpResponseMessage response2 = await client.GetAsync(PersonalInforsUrl);
                //response2.EnsureSuccessStatusCode();

                //string strData2 = await response2.Content.ReadAsStringAsync();
                //List<PersonalInfor> personalInfor = System.Text.Json.JsonSerializer.Deserialize<List<PersonalInfor>>(strData2, options);
                //// If user is neither admin nor regular member, return to login view
                Console.WriteLine("ok");
                return RedirectToActionPermanent("Index", "Home");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterDto registerInfor)
        {
            string data = JsonConvert.SerializeObject(registerInfor);
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync("https://localhost:7051/api/PersonalInfors/register", content).Result;

            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Login", "Authen");
            }
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> ForgotPassword()
        {
            string ok = HttpContext.Session.GetString("Token");

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> ForgotPassword([FromForm] string email)
        {
            string data = JsonConvert.SerializeObject(email);
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
            HttpResponseMessage message = client.PostAsync(PersonalInforsUrl + "/forgot-password?email=" + email, content).Result;
            if (message.IsSuccessStatusCode)
            {
                return RedirectToAction("SendEmailSuccess", "Authen");
            }
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> SendEmailSuccess()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> ResetPassword()
        {
            ResetPasswordViewModel model = new ResetPasswordViewModel();
            model.Email = Request.Query["email"];
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            string data = JsonConvert.SerializeObject(model);
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
            HttpResponseMessage message = client.PostAsync("https://localhost:7051/api/PersonalInfors/reset-password?email=" + model.Email, content).Result;
            if (message.IsSuccessStatusCode)
            {
                return RedirectToAction("Login", "Authen");
            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            HttpContext.Session.Clear();

            return RedirectToActionPermanent("Index", "Home");
        }

    }
}
