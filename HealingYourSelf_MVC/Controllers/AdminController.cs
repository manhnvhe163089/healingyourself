﻿using HealingYourSelf_MVC.Dto;
using HealingYourSelf_MVC.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace HealingYourSelf_MVC.Controllers
{
    public class AdminController : Controller
    {
        private readonly HttpClient client;

        public AdminController()
        {
            client = new HttpClient();
            var contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);
        }

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Home()
        {
            return View();
        }

        [HttpGet]
        public IActionResult ManageOperator()
        {

            List<OperatorInfor> manageOperator = new List<OperatorInfor>();
            HttpResponseMessage response = client.GetAsync("https://localhost:7051/api/Admin/GetAllOperator").Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                manageOperator = JsonConvert.DeserializeObject<List<OperatorInfor>>(data);
            }

            var listOperator = manageOperator.ToList();

            return View(listOperator);
        }

        [HttpGet]
        public IActionResult ViewDetailOperator(int id)
        {

            List<OperatorInfor> manageOperator = new List<OperatorInfor>();
            HttpResponseMessage response = client.GetAsync("https://localhost:7051/api/Admin/DetailOperator/").Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                manageOperator = JsonConvert.DeserializeObject<List<OperatorInfor>>(data);
            }

            var listOperator = manageOperator.ToList();

            return View(listOperator);
        }
    }
}
